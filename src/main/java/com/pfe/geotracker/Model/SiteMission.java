package com.pfe.geotracker.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class SiteMission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private Site sites;
    @JsonIgnore
    @ManyToOne
    private Mission missionsites;
    @Column
    private int orderNumber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Site getSites() {
        return sites;
    }

    public void setSites(Site sites) {
        this.sites = sites;
    }

    public Mission getMissionsites() {
        return missionsites;
    }

    public void setMissionsites(Mission missionsites) {
        this.missionsites = missionsites;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }
}
