package com.pfe.geotracker.Controller;


import com.pfe.geotracker.Model.Mission;
import com.pfe.geotracker.Model.Truck;
import com.pfe.geotracker.Model.TruckPageanable;
import com.pfe.geotracker.service.TruckService;
import com.pfe.geotracker.utils.PaginatedList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Locale;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/")
public class TruckController {

    @Autowired
    private TruckService truckService;


    //get all trucks Rest api
    @GetMapping("/trucks")
    public List<Truck> getAlltrucks() {

        return truckService.getAllTruck();
    }


    //get all trucks pageanable Rest api
    @GetMapping("/trucks/pageanable")
    public TruckPageanable getAllTrucks(@RequestParam int pageSize, @RequestParam int currentPage, @RequestParam String search, @RequestParam String orderBy) {
        List<Truck> trucks = null;
        if (orderBy == null || orderBy.isEmpty()) {
            trucks = truckService.getAllTruck();
        } else {
            trucks = truckService.getAllTruckOrderBy(orderBy);
        }
        PaginatedList<Truck> paginatedList = new PaginatedList(trucks, pageSize);
        TruckPageanable<Truck> truckPageanable = new TruckPageanable<>();
        truckPageanable.setCurrentPage(currentPage);
        truckPageanable.setPageSize(pageSize);
        truckPageanable.setOrderBy(orderBy.toLowerCase());
        truckPageanable.setSearch(search.toLowerCase());
        truckPageanable.setTotalPage(trucks.size() / pageSize);
        truckPageanable.setTotalItem(trucks.size());

        truckPageanable.setData(paginatedList.getPage(currentPage));
        return truckPageanable;
    }

    //Create truck Rest api
    @PostMapping("/truck")
    public Truck createTruck(@RequestBody Truck truck) {

        return truckService.addTruck(truck);
    }


    //get truck by id Rest api
    @GetMapping("/truck")
    public Truck getTruckById(@RequestParam long id) {
        return truckService.getByIDTruck(id);
    }


    //update truck Rest api
    @PutMapping("/truck")
    public Truck updateTruck(@RequestBody Truck truck) {

        return truckService.updateTruck(truck);

    }


    //Delete truck Rest api
    @DeleteMapping("/truck")
    public long deleteTruck(@RequestParam long id) {
        return truckService.deleteTruck(id);
    }


}
