package com.pfe.geotracker.Model;

import javax.persistence.*;

@Entity
@Table(name = "alerts")
public class Alert {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "description")
    private String description;
    @Column(name = "type")
    private String type;


}
