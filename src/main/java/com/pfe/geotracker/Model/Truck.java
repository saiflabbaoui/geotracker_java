package com.pfe.geotracker.Model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "trucks")
public class Truck {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "registration_number")
    private String registrationNumber;
    @Column(name = "model")
    private String model;
    @Column(name = "power")
    private String power;
    @Column(name = "status")
    private String status;

    @OneToOne(mappedBy = "truck", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private TruckInformation truckinformation;
    @JsonIgnore
    @OneToMany(mappedBy = "truck", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<Driver> drivers;


    public Truck() {
    }

    public Truck(String registrationNumber, String model, String power, TruckInformation truckinformation, List<Driver> drivers) {
        this.registrationNumber = registrationNumber;
        this.model = model;
        this.power = power;
        this.truckinformation = truckinformation;
        this.drivers = drivers;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getStatus() {  return status;  }

    public void setStatus(String status) { this.status = status; }

    public TruckInformation getTruckinformation() {
        return truckinformation;
    }

    public void setTruckinformation(TruckInformation truckinformation) {
        this.truckinformation = truckinformation;
    }

    public List<Driver> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<Driver> drivers) {
        this.drivers = drivers;
    }


}
