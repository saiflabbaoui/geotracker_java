package com.pfe.geotracker.Controller;

import com.pfe.geotracker.Model.Company;
import com.pfe.geotracker.Model.Driver;
import com.pfe.geotracker.service.CompanyService;
import com.pfe.geotracker.service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    //get all companies Rest api
    @GetMapping("/companies")
    public List<Company> getAllCompanies() {

        return companyService.getAllCompany();
    }
}
