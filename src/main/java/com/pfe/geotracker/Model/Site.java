package com.pfe.geotracker.Model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jdk.nashorn.internal.ir.annotations.Ignore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "sites")
public class Site {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "latitude")
    private long latitude;
    @Column(name = "longitude")
    private long Longitude;
    @Column(name = "type")
    private String Type;
    @Column(name = "name")
    private String Name;
    @JsonIgnore
    @OneToMany(mappedBy = "sites", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<SiteMission> sites;

    public Site() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLatitude() {
        return latitude;
    }

    public void setLatitude(long latitude) {
        this.latitude = latitude;
    }

    public long getLongitude() {
        return Longitude;
    }

    public void setLongitude(long longitude) {
        Longitude = longitude;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public List<SiteMission> getSites() {
        return sites;
    }

    public void setSites(List<SiteMission> sites) {
        this.sites = sites;
    }
}
