package com.pfe.geotracker.service;

import com.pfe.geotracker.Model.Driver;
import com.pfe.geotracker.Model.Truck;

import java.util.List;

public interface DriverService {
    Driver addDriver(Driver driver);

    Driver updateDriver(Driver driver);

    Driver getByIDDriver(long driverId);

    long deleteDriver(long driverId);

    List<Driver> getAllDriver();

     List<Driver> getAllDriverOrderBy(String orderBY) ;

}
