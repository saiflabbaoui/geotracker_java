package com.pfe.geotracker.Model;

import java.util.List;

public class MissionSite {
    private Mission mission;
    private List<Site> sites;

    public MissionSite() {
    }

    public Mission getMission() {
        return mission;
    }

    public void setMission(Mission mission) {
        this.mission = mission;
    }

    public List<Site> getSites() {
        return sites;
    }

    public void setSites(List<Site> sites) {
        this.sites = sites;
    }
}
