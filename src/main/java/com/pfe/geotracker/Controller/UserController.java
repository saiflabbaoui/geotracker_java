package com.pfe.geotracker.Controller;

import com.pfe.geotracker.Model.Company;
import com.pfe.geotracker.Model.User;
import com.pfe.geotracker.service.CompanyService;
import com.pfe.geotracker.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/")
public class UserController {

    @Autowired
    private UserService userService;


    //get all usersByCompany Rest api
    @GetMapping("/users")
    public List<User> getAllUsersByCompany(@RequestParam long idCompany) {

        return userService.getUsersByCompany(idCompany);
    }
}
