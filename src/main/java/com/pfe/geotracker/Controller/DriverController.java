package com.pfe.geotracker.Controller;


import com.pfe.geotracker.Model.*;
import com.pfe.geotracker.service.DriverService;
import com.pfe.geotracker.utils.PaginatedList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;



@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/")
public class DriverController {

    @Autowired
    private DriverService driverService;

    //get all drivers Rest api
    @GetMapping("/drivers")
    public List<Driver> getAlldrivers() {

        return driverService.getAllDriver();
    }



    //get all pageanable drivers Rest api
    @GetMapping("/drivers/pageanable")
    public DriverPageanable getAllDrivers(@RequestParam int pageSize, @RequestParam int currentPage, @RequestParam String search, @RequestParam String orderBy) {
        List<Driver> drivers = null;
        if (orderBy == null || orderBy.isEmpty()) {
            drivers = driverService.getAllDriver();
        } else {
            drivers = driverService.getAllDriverOrderBy(orderBy);
        }
        PaginatedList<Driver> paginatedList = new PaginatedList(drivers, pageSize);
        DriverPageanable<Driver> driverPageanable = new DriverPageanable<>();
        driverPageanable.setCurrentPage(currentPage);
        driverPageanable.setPageSize(pageSize);
        driverPageanable.setOrderBy(orderBy.toLowerCase());
        driverPageanable.setSearch(search);
        driverPageanable.setTotalPage(drivers.size() / pageSize);
        driverPageanable.setTotalItem(drivers.size());

        driverPageanable.setData(paginatedList.getPage(currentPage));
        return driverPageanable;
    }

    //Create driver Rest api
    @PostMapping("/driver")
    public Driver createDriver(@RequestBody Driver driver) {
        return driverService.addDriver(driver);
    }


    //get driver by id Rest api
    @GetMapping("/driver/{id}")
    public Driver getDriverById(@RequestParam long id) {
     return driverService.getByIDDriver(id);
    }


    //update driver Rest api
    @PutMapping("/driver")
    public Driver updateDriver( @RequestBody Driver driver) {
      return driverService.updateDriver(driver);
    }


    //Delete driver Rest api
    @DeleteMapping("/driver")
    public long deleteDriver(@RequestParam long id) {
     return driverService.deleteDriver(id);
    }

    public void AffectDriverToMission(Driver driver, Mission mission, int idReclamation) {
    }


}
