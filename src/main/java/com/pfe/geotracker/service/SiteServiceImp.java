package com.pfe.geotracker.service;

import com.pfe.geotracker.Model.Mission;
import com.pfe.geotracker.Model.Site;
import com.pfe.geotracker.Model.SiteMission;
import com.pfe.geotracker.Repository.SiteMissionRepository;
import com.pfe.geotracker.Repository.SiteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class SiteServiceImp implements SiteService{

    @Autowired
    SiteRepository siteRepository;

    @Autowired
    SiteMissionRepository siteMissionRepository;




    @Override
    public Site addSite(Site site) {
        return siteRepository.save(site);
    }

    @Override
    public void addSiteToMission(Mission mission, List<Site> sites) {
        int o=1;
        for (Site s:sites) {
            SiteMission siteMission=new SiteMission();
            siteMission.setMissionsites(mission);
            siteMission.setSites(s);
            siteMission.setOrderNumber(o);
            o++;
            siteMissionRepository.save(siteMission);
        }
    }

    @Override
    public List<SiteMission> getAllSites() {
        return siteMissionRepository.findAll();
    }

    @Override
    public List<Site> getAll() {
        return siteRepository.findAll();
    }
}
