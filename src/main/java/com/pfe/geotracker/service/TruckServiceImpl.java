package com.pfe.geotracker.service;

import com.pfe.geotracker.Model.Driver;
import com.pfe.geotracker.Model.Mission;
import com.pfe.geotracker.Model.Truck;
import com.pfe.geotracker.Repository.TruckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TruckServiceImpl implements TruckService {


    @Autowired
    TruckRepository truckRepository;

    @Autowired
    DriverService driverService;
    @Autowired
    MissionService missionService;


    @Override
    public Truck addTruck(Truck truck) {
        truck.setStatus("Free");
        return truckRepository.save(truck);
    }

    @Override
    public Truck updateTruck(Truck truck) {
        Optional<Truck> oldTruck = truckRepository.findById(truck.getId());
        if (oldTruck.isPresent()) {
            oldTruck.get().setDrivers(truck.getDrivers());
            oldTruck.get().setPower(truck.getPower());
            oldTruck.get().setModel(truck.getModel());
            oldTruck.get().setStatus(truck.getStatus());
            oldTruck.get().setTruckinformation(truck.getTruckinformation());
            oldTruck.get().setRegistrationNumber(truck.getRegistrationNumber());
        }
        truckRepository.save(oldTruck.get());
        return oldTruck.get();
    }

    @Override
    public Truck getByIDTruck(long truckId) {
        return truckRepository.findById(truckId).get();
    }

    @Override
    public long deleteTruck(long truckId) {
        Optional<Truck> oldTruck = truckRepository.findById(truckId);
        truckRepository.delete(oldTruck.get());
        return oldTruck.get().getId();
    }

    @Override
    public List<Truck> getAllTruck() {
        return truckRepository.findAll();
    }

    @Override
    public List<Truck> getAllTruckOrderBy(String orderBY) {
        return truckRepository.findAll(Sort.by(Sort.Direction.ASC, orderBY));
    }
}
