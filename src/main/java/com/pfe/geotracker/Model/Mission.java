package com.pfe.geotracker.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@Entity
@Table(name = "missions")
public class Mission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "start_date")
    private String startDate;
    @Column(name = "end_date")
    private String endDate;
    @Column(name = "description")
    private String description;


    @OneToMany(mappedBy = "mission", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
        private List<Driver> drivers;
    @OneToMany(mappedBy = "missionsites", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<SiteMission> missionsites;
    transient List<Site> sites;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Driver> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<Driver> drivers) {
        this.drivers = drivers;
    }

    public List<SiteMission> getMissionsites() {
        return missionsites;
    }

    public void setMissionsites(List<SiteMission> missionsites) {
        this.missionsites = missionsites;
    }

    public List<Site> getSites() {
        return sites;
    }

    public void setSites(List<Site> sites) {
        this.sites = sites;
    }

}
