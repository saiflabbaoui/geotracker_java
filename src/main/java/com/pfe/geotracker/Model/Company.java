package com.pfe.geotracker.Model;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "companies")
public class Company {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "social_reason")
    private String socialReason;
    @Column(name = "nbr_users")
    private int nbrUsers;
    @Column(name = "nbr_trucks")
    private int nbrTrucks;
    @Column(name = "address")
    private String address;
    @Column(name = "phoneNumber")
    private String phoneNumber;
    @OneToMany(mappedBy = "company", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<Driver> drivers;
    @OneToMany(mappedBy = "company", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<User> users;

    public Company() {
    }

    public Company(String socialReason, int nbrUsers, int nbrTrucks, String address, String phoneNumber) {
        this.socialReason = socialReason;
        this.nbrUsers = nbrUsers;
        this.nbrTrucks = nbrTrucks;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSocialReason() {
        return socialReason;
    }

    public void setSocialReason(String socialReason) {
        this.socialReason = socialReason;
    }

    public int getNbrUsers() {
        return nbrUsers;
    }

    public void setNbrUsers(int nbrUsers) {
        this.nbrUsers = nbrUsers;
    }

    public int getNbrTrucks() {
        return nbrTrucks;
    }

    public void setNbrTrucks(int nbrTrucks) {
        this.nbrTrucks = nbrTrucks;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


}
