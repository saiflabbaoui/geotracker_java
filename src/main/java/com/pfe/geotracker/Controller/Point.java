package com.pfe.geotracker.Controller;


import com.pfe.geotracker.service.MissionServiceImpl;

public  class Point {
    private long id;
    private double longitude;
    private double latitude;
    private double speed;
    private double fuellevel;

    public long getId() {
        return id;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getSpeed() {
        return speed;
    }

    public double getFuellevel() {
        return fuellevel;
    }



}