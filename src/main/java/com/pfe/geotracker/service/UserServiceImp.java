package com.pfe.geotracker.service;

import com.pfe.geotracker.Model.Company;
import com.pfe.geotracker.Model.Driver;
import com.pfe.geotracker.Model.Truck;
import com.pfe.geotracker.Model.User;
import com.pfe.geotracker.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImp implements UserService{
    @Autowired
    UserRepository userRepository;

    @Override
    public List<User> getUsersByCompany(long id) {
        List<User> users = userRepository.findAll();
        List<User> usersCompany = new ArrayList<>();
        for (User u : users) {
            if (u.getCompany().getId() == id) {
                usersCompany.add(u);
            }
        }
        return usersCompany;    }
}




