package com.pfe.geotracker.service;

import com.pfe.geotracker.Model.Mission;
import com.pfe.geotracker.Model.Site;
import com.pfe.geotracker.Model.SiteMission;

import java.util.List;

public interface SiteService {

    Site addSite(Site site);
    void addSiteToMission(Mission mission, List<Site>  site);

   // Site updateSite(Site site);

   // Site getByIDSite(long siteId);

   // long deleteSite(long siteId);

    List<SiteMission> getAllSites();

    List<Site> getAll();


}
