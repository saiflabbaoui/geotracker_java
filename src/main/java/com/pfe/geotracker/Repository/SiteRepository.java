package com.pfe.geotracker.Repository;

import com.pfe.geotracker.Model.Site;
import com.pfe.geotracker.Model.Truck;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface SiteRepository extends JpaRepository<Site, Long> {
}
