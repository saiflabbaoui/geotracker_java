package com.pfe.geotracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeotrackerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeotrackerApplication.class, args);
    }

}
