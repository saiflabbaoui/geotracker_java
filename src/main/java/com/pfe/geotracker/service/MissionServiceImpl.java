package com.pfe.geotracker.service;

import com.pfe.geotracker.Model.*;
import com.pfe.geotracker.Repository.DriverRepository;
import com.pfe.geotracker.Repository.MissionRepository;
import com.pfe.geotracker.Repository.TruckRepository;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MissionServiceImpl implements MissionService {
    @Autowired
    DriverRepository driverRepository;

    @Autowired
    MissionRepository missionRepository;

    @Autowired
    TruckRepository truckRepository;

    @Autowired
    DriverService driverService;
    @Autowired
    TruckService truckService;
    @Autowired
    SiteService siteService;

    @Override
    public Mission addMission(Truck truck, Mission mission, List<Driver> drivers, List<Site> sites) {
        List<Truck> trucks = new ArrayList<>();
        mission.setDrivers(null);
        Mission m = missionRepository.save(mission);
        //ajouter trajet au service geoapi
        this.addtrajet(m, drivers.get(0).getTruck());
        siteService.addSiteToMission(mission, sites);
        truck.setDrivers(drivers);
        truck.setStatus("OnMission");
        truckService.updateTruck(truck);
        for (Driver d : drivers) {
            d.setMission(m);
            d.setTruck(truckService.getByIDTruck(truck.getId()));
            driverService.updateDriver(d);
        }


        return this.updateMission(m);
    }

    @Override
    public Mission updateMission(Mission mission) {
        Optional<Mission> oldMission = missionRepository.findById(mission.getId());
        if (oldMission.isPresent()) {
            oldMission.get().setDescription(mission.getDescription());
            oldMission.get().setDrivers(mission.getDrivers());
            oldMission.get().setStartDate(mission.getStartDate());
            oldMission.get().setEndDate(mission.getEndDate());
            //   oldMission.get().setSites(mission.getSites());

        }
        mission = missionRepository.save(oldMission.get());

        return oldMission.get();
    }

    @Override
    public Mission getByIDMission(long missionId) {
        return missionRepository.findById(missionId).get();
    }

    @Override
    public long deleteMission(long missionId) {
        Optional<Mission> oldMission = missionRepository.findById(missionId);
        missionRepository.delete(oldMission.get());
        return oldMission.get().getId();
    }

    @Override
    public List<Mission> getAllMission() {
        List<Mission> missions = missionRepository.findAll();
        for (Mission m : missions) {
            List<Site> currentSite = new ArrayList<>();
            for (SiteMission s : m.getMissionsites()) {
                currentSite.add(s.getSites());
            }
            m.setSites(currentSite);
        }

        return missions;
    }

    @Override
    public Truck getTuckByMission(Mission mission) {
        List<Driver> drivers = driverRepository.findAll();
        Truck truck = null;
        for (Driver d : drivers) {
            if (d.getMission().getId() == mission.getId()) {
                truck = d.getTruck();
            }
        }
        return truck;
    }

    @Override
    public List<Driver> getAllDriversByMission(Mission mission) {
        List<Driver> drivers = driverRepository.findAll();
        List<Driver> selectedDrivers = new ArrayList<>();
        for (Driver d : drivers) {
            if (d.getMission().getId() == mission.getId()) {
                selectedDrivers.add(d);
            }
        }
        return selectedDrivers;
    }

    @Override
    public List<Mission> getAllMissionOrderBy(String orderBY) {
        return missionRepository.findAll(Sort.by(Sort.Direction.ASC, orderBY));
    }

    public void addtrajet(Mission mission, Truck truck) {
        Trajet t = new Trajet();
        t.setId(mission.getId());
        t.setTruck(truck.getId());
        t.setMissionName(mission.getDescription());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Trajet> httpEntity = new HttpEntity<>(t, httpHeaders);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForObject("http://localhost:8083/geopoint/addTrajet",httpEntity, Trajet.class);
    }

    private class Trajet {
        private long id;
        private long truck;
        private String missionName;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public long getTruck() {
            return truck;
        }

        public void setTruck(long truck) {
            this.truck = truck;
        }

        public String getMissionName() {
            return missionName;
        }

        public void setMissionName(String missionName) {
            this.missionName = missionName;
        }
    }

    public static class Point {
        private long id;
        private double longitude;
        private double latitude;
        private double speed;
        private double fuellevel;
        private Trajet trajet;

        public long getId() {
            return id;
        }

        public double getLongitude() {
            return longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getSpeed() {
            return speed;
        }

        public double getFuellevel() {
            return fuellevel;
        }

        public Trajet getTrajet() {
            return trajet;
        }

    }

}
