package com.pfe.geotracker.service;

import com.pfe.geotracker.Model.Driver;
import com.pfe.geotracker.Model.Mission;
import com.pfe.geotracker.Model.Site;
import com.pfe.geotracker.Model.Truck;

import java.util.List;

public interface MissionService {
    Mission addMission(Truck truck, Mission mission, List<Driver> driver, List<Site> sites);

    Mission updateMission(Mission mission);

    Mission getByIDMission(long missionId);

    long deleteMission(long missionId);

    List<Mission> getAllMission();
    Truck getTuckByMission(Mission mission);
    List<Driver> getAllDriversByMission(Mission mission);
    public List<Mission> getAllMissionOrderBy(String orderBY) ;

}
