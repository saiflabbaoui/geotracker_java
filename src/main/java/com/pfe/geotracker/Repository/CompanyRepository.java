package com.pfe.geotracker.Repository;

import com.pfe.geotracker.Model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Long> {
}
