package com.pfe.geotracker.Controller;

import com.pfe.geotracker.Model.Mission;
import com.pfe.geotracker.Model.MissionPageanable;
import com.pfe.geotracker.Model.MissionSite;
import com.pfe.geotracker.Model.TruckInformation;
import com.pfe.geotracker.Repository.TruckInformationRepository;
import com.pfe.geotracker.service.MissionService;
import com.pfe.geotracker.service.SiteService;
import com.pfe.geotracker.utils.PaginatedList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/")
public class MissionController {

    @Autowired
    TruckInformationRepository truckInformationRepository;
    @Autowired
    private MissionService missionService;
    @Autowired
    private SiteService siteService;

    //get all missions Rest api
    @GetMapping("/missions")
    public List<Mission> getAllMissions()  {
        List<Mission> missions = missionService.getAllMission();
        for (Mission m : missions) {
            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
            LocalDateTime ldt = LocalDateTime.parse(m.getStartDate(), inputFormatter);
            LocalDateTime lt = LocalDateTime.now();
            if (ldt.toEpochSecond(ZoneOffset.UTC) - lt.toEpochSecond(ZoneOffset.UTC)>0)
            new ReminderMission((int) (ldt.toEpochSecond(ZoneOffset.UTC) - lt.toEpochSecond(ZoneOffset.UTC)), m);
        }

        return missions;
    }

    @PostMapping("/startmission")
    public void startmission(@RequestBody Point point, UriComponentsBuilder builder) {
        TruckInformation ti = new TruckInformation();
        ti.setDate(new Date());
        ti.setLatitude(point.getLatitude());
        ti.setLongitude(point.getLongitude());
        ti.setSpeed(point.getSpeed());
        truckInformationRepository.save(ti);
        System.out.println(point);
    }

    //get all missions pageanable Rest api
    @GetMapping("/missions/pageanable")
    public MissionPageanable getAllMissions(@RequestParam int pageSize, @RequestParam int currentPage, @RequestParam String search, @RequestParam String orderBy) {
        List<Mission> missions = null;
        if (orderBy == null || orderBy.isEmpty()) {
            missions = missionService.getAllMission();

        } else {
            missions = missionService.getAllMissionOrderBy(orderBy);
        }
        PaginatedList<Mission> paginatedList = new PaginatedList(missions, pageSize);
        MissionPageanable<Mission> missionPageanable = new MissionPageanable<>();
        missionPageanable.setCurrentPage(currentPage);
        missionPageanable.setPageSize(pageSize);
        missionPageanable.setOrderBy(orderBy.toLowerCase());
        missionPageanable.setSearch(search);
        missionPageanable.setTotalPage(missions.size() / pageSize);
        missionPageanable.setTotalItem(missions.size());

        missionPageanable.setData(paginatedList.getPage(currentPage));
        return missionPageanable;
    }


    //Create mission Rest api
    @PostMapping("/mission")
    public Mission createMission(@Valid @RequestBody MissionSite ms) {
        return missionService.addMission(ms.getMission().getDrivers().get(0).getTruck(), ms.getMission(), ms.getMission().getDrivers(), ms.getSites());
    }


    //get mission by id Rest api
    @GetMapping("/mission")
    public Mission getMissionById(@RequestParam long id) {
        return missionService.getByIDMission(id);
    }


    //update mission Rest api
    @PutMapping("/mission")
    public Mission updateMission(@RequestBody Mission mission) {

        return missionService.updateMission(mission);

    }


    //Delete mission Rest api
    @DeleteMapping("/mission")
    public long deleteMission(@RequestParam long id) {
        return missionService.deleteMission(id);
    }

    private class Requete {
        private String lien;

        private long idTrajet;

        public String getLien() {
            return this.lien;
        }

        public void setLien(String lien) {
            this.lien = lien;
        }

        public long getIdTrajet() {
            return this.idTrajet;
        }

        public void setIdTrajet(long idTrajet) {
            this.idTrajet = idTrajet;
        }
    }

    private class ReminderMission {
        Timer timer;
        Mission m;

        public ReminderMission(int seconds, Mission m) {
            timer = new Timer();
            this.m = m;
            timer.schedule(new RemindTask(), seconds * 1000);
        }

        class RemindTask extends TimerTask {
            public void run() {
                Requete requete = new Requete();
                requete.setIdTrajet(m.getId());
                requete.setLien("http://localhost:8080/api/v1/startmission");
                // call for start method in geo api
            }
        }
    }

}
