package com.pfe.geotracker.Controller;

import com.pfe.geotracker.Model.Company;
import com.pfe.geotracker.Model.CompanyAdmin;
import com.pfe.geotracker.Model.RoleUser;
import com.pfe.geotracker.Model.User;
import com.pfe.geotracker.PayLoad.Request.LoginRequest;
import com.pfe.geotracker.PayLoad.Request.SignupRequest;
import com.pfe.geotracker.PayLoad.Response.JwtResponse;
import com.pfe.geotracker.PayLoad.Response.MessageResponse;
import com.pfe.geotracker.Repository.CompanyRepository;
import com.pfe.geotracker.Repository.UserRepository;
import com.pfe.geotracker.Security.Jwt.JwtUtils;
import com.pfe.geotracker.Security.Services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;
//
//    @Autowired
//    RoleUserRepository roleRepository;

    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUserName(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles));
    }

    @PostMapping("/signup")
//    @PreAuthorize("ROLE_ADMIN")
    public ResponseEntity<?> registerUser(@RequestBody CompanyAdmin companyAdmin) {
        if (userRepository.existsByUserName(companyAdmin.getUser().getUserName())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(companyAdmin.getUser().getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        User u = new User(companyAdmin.getUser().getFirstName(), companyAdmin.getUser().getLastName(), companyAdmin.getUser().getUserName(),
                companyAdmin.getUser().getEmail(),
                encoder.encode(companyAdmin.getUser().getPassword()), RoleUser.ROLE_USER,companyRepository.save(companyAdmin.getCompany()));
        userRepository.save(u);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
}
