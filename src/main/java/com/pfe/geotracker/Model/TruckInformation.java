package com.pfe.geotracker.Model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "truckinformation")
public class TruckInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "latitude")
    private double latitude;
    @Column(name = "longitude")
    private double longitude;
    @Column(name = "date")
    @OrderBy("current_date asc ")
    private Date date;
    @Column(name = "speed")
    private double speed;
    @Column(name = "fuel_level")
    private long fuelLevel;

    @OneToOne
    private Truck truck;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public long getFuelLevel() {
        return fuelLevel;
    }

    public void setFuelLevel(long fuelLevel) {
        this.fuelLevel = fuelLevel;
    }

    public Truck getTruck() {
        return truck;
    }

    public void setTruck(Truck truck) {
        this.truck = truck;
    }
}
