package com.pfe.geotracker.service;

import com.pfe.geotracker.Model.Company;
import com.pfe.geotracker.Model.Driver;
import com.pfe.geotracker.Repository.CompanyRepository;
import com.pfe.geotracker.Repository.DriverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyServiceImp implements CompanyService{

    @Autowired
    CompanyRepository companyRepository;

    @Override
    public Company addCompany(Company company) {
        return companyRepository.save(company);
    }

    @Override
    public Company updateCompany(Company company) {
        Optional<Company> oldCompany = companyRepository.findById(company.getId());
        if(oldCompany.isPresent()){
            oldCompany.get().setSocialReason(company.getSocialReason());
            oldCompany.get().setNbrTrucks(company.getNbrTrucks());
            oldCompany.get().setNbrUsers(company.getNbrUsers());
        }
        companyRepository.save(oldCompany.get());
        return oldCompany.get();
    }

    @Override
    public Company getByIDCompany(long companyId) {
        return companyRepository.findById(companyId).get();
    }

    @Override
    public long deleteCompany(long companyId) {
        Optional<Company> oldCompany = companyRepository.findById(companyId);
        if (oldCompany.isPresent()) {
            companyRepository.delete(oldCompany.get());
        }
        return oldCompany.get().getId();
    }

    @Override
    public List<Company> getAllCompany() {
        return companyRepository.findAll();
    }
}
