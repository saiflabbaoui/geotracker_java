package com.pfe.geotracker.Repository;

import com.pfe.geotracker.Model.Site;
import com.pfe.geotracker.Model.SiteMission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface SiteMissionRepository extends JpaRepository<SiteMission, Long> {
}
