package com.pfe.geotracker.service;

import com.pfe.geotracker.Model.Driver;
import com.pfe.geotracker.Model.Truck;
import com.pfe.geotracker.Repository.DriverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DriverServiceImpl implements DriverService {
    @Autowired
    DriverRepository driverRepository;



    @Override
    public Driver addDriver(Driver driver) {
        return driverRepository.save(driver);
    }

    @Override
    public Driver updateDriver(Driver driver) {
        Optional<Driver> oldDriver = driverRepository.findById(driver.getId());
        if (oldDriver.isPresent()) {
            oldDriver.get().setFirstName(driver.getFirstName());
            oldDriver.get().setLastName(driver.getLastName());
            oldDriver.get().setPhoneNumber(driver.getPhoneNumber());
            oldDriver.get().setAddress(driver.getAddress());
            oldDriver.get().setEmail(driver.getEmail());
            oldDriver.get().setTruck(driver.getTruck());
        }
        driverRepository.save(oldDriver.get());
        return oldDriver.get();
    }

    @Override
    public Driver getByIDDriver(long driverId) {
        return driverRepository.findById(driverId).get();
    }

    @Override
    public long deleteDriver(long driverId) {
        Optional<Driver> oldDriver = driverRepository.findById(driverId);
        if (oldDriver.isPresent()) {
            driverRepository.delete(oldDriver.get());
        }
        return oldDriver.get().getId();

    }

    @Override
    public List<Driver> getAllDriver() {
        return driverRepository.findAll();
    }

    @Override
    public List<Driver> getAllDriverOrderBy(String orderBY) {
        return driverRepository.findAll(Sort.by(Sort.Direction.ASC, orderBY));
    }


}
