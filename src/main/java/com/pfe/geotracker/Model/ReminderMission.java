package com.pfe.geotracker.Model;


import java.util.Timer;
import java.util.TimerTask;

public class ReminderMission {
    Timer timer;

    public ReminderMission(int seconds) {
        timer = new Timer();
        timer.schedule(new RemindTask(), seconds * 1000);
    }

    public static void main(String[] args) {
        new ReminderMission(5);
        System.out.println("Task scheduled.");
    }

    class RemindTask extends TimerTask {
        public void run() {
            System.out.println("Time's up!");
            timer.cancel(); //Terminate the timer thread
        }
    }
}
