package com.pfe.geotracker.service;


import com.pfe.geotracker.Model.Truck;

import java.util.List;

public interface TruckService {
    Truck addTruck(Truck truck);

    Truck updateTruck(Truck truck);

    Truck getByIDTruck(long truckId);

    long deleteTruck(long truckId);

    List<Truck> getAllTruck();
    public List<Truck> getAllTruckOrderBy(String orderBY) ;


    }
