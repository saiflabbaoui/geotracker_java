package com.pfe.geotracker.PayLoad.Response;

import java.util.List;

public class JwtResponse {

    private String token;
    private String type = "Bearer";
    private long id;
    private String userName;
    private String email;
    private List<String> roles;

    public JwtResponse(String token, long id, String userName, String email, List<String> roles) {
        this.token = token;
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.roles = roles;
    }

    public String getAccessToken() {
        return token;
    }

    public void setAccessToken(String accessType) {
        this.token = accessType;
    }

    public String getTokenType() {
        return type;
    }

    public void setTokenType(String TokenType) {
        this.type = TokenType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getuserName() {
        return userName;
    }

    public void setuserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
