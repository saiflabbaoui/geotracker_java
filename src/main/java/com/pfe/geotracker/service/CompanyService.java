package com.pfe.geotracker.service;

import com.pfe.geotracker.Model.Company;

import java.util.List;

public interface CompanyService {

    Company addCompany(Company company);

    Company updateCompany(Company company);

    Company getByIDCompany(long companyId);

    long deleteCompany(long companyId);

    List<Company> getAllCompany();
}
