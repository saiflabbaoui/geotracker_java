package com.pfe.geotracker.Controller;

import com.pfe.geotracker.Model.Truck;
import com.pfe.geotracker.Model.TruckInformation;
import com.pfe.geotracker.Repository.TruckInformationRepository;
import com.pfe.geotracker.service.MissionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Date;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {
    @Autowired
    TruckInformationRepository truckInformationRepository;
    @GetMapping("/all")
    public String allAccess() {
        return "Public Content.";
    }

    @GetMapping("/user")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public String userAccess() {
        return "User Content.";
    }


    @GetMapping("/admin")
    @PreAuthorize("hasRole('ADMIN')")
    public String adminAccess() {
        return "Admin Board.";
    }


    @PostMapping("/test")
    public void test(@RequestBody Point point, UriComponentsBuilder builder ){
        TruckInformation ti=new TruckInformation();
        ti.setDate(new Date());
        ti.setLatitude(point.getLatitude());
        ti.setLongitude(point.getLongitude());
        ti.setSpeed(point.getSpeed());
        truckInformationRepository.save(ti);
        System.out.println(point.getLatitude());
    }

}
