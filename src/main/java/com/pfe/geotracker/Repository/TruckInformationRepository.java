package com.pfe.geotracker.Repository;

import com.pfe.geotracker.Model.Truck;
import com.pfe.geotracker.Model.TruckInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TruckInformationRepository extends JpaRepository<TruckInformation, Long> {
}
