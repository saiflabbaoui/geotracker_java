package com.pfe.geotracker.Controller;


import com.pfe.geotracker.Model.Site;
import com.pfe.geotracker.Model.Truck;
import com.pfe.geotracker.Model.TruckPageanable;
import com.pfe.geotracker.service.SiteService;
import com.pfe.geotracker.service.TruckService;
import com.pfe.geotracker.utils.PaginatedList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/")
public class SiteController {

    @Autowired
    private SiteService siteService;


    //get all sites Rest api
    @GetMapping("/sites")
    public List<Site> getAlltrucks() {

        return siteService.getAll();
    }



    //Create site Rest api
    @PostMapping("/site")
    public Site createTruck(@RequestBody Site site) {

        return siteService.addSite(site);
    }


}
