package com.pfe.geotracker.service;

import com.pfe.geotracker.Model.Company;
import com.pfe.geotracker.Model.Truck;
import com.pfe.geotracker.Model.User;

import java.util.List;

public interface UserService {

     List<User> getUsersByCompany(long id);

}
