package com.pfe.geotracker.Model;


import javax.persistence.*;

@Entity
@Table(name = "workSpaces")
public class WorkSpace {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "start_date")
    private String startDate;
    @Column(name = "end_date")
    private String endDate;

}
